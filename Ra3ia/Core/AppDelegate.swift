//
//  AppDelegate.swift
//  Ra3ia
//
//  Created by Sara Mady on 21/03/2021.
//

import UIKit
import IQKeyboardManagerSwift
import GooglePlaces
import GoogleMaps

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    static var FCMTOKEN = "1234567"
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        self.setUpSegment()
        
        GMSServices.provideAPIKey("AIzaSyBrEyBXL7xP6XwzTnybqmYciGjjxsYF5xI")
        GMSPlacesClient.provideAPIKey("AIzaSyBrEyBXL7xP6XwzTnybqmYciGjjxsYF5xIs")
        
        
        if User.currentUser != nil , KeyChain.userToken != nil {
            if KeyChain.tokenExist{
                GoToHomeStoryBoard()
            }else{
                GoToAuthntcatonStoryBoard()
            }
        }else{
            GoToAuthntcatonStoryBoard()
        }



       return true
   }

    func GoToAuthntcatonStoryBoard(){
        let vc = UIStoryboard.instantiateInitialViewController(.Authntication)
        self.window?.rootViewController = vc
        window?.makeKeyAndVisible()
    }
    func GoToHomeStoryBoard(){
        let vc = UIStoryboard.instantiateInitialViewController(.Main)
        self.window?.rootViewController = vc
        window?.makeKeyAndVisible()
    }
    
}
extension AppDelegate {
    
    
    
    
    func setUpSegment() {
        
        
        if let font = UIFont(name: "Fairuz", size: 16 )
        {
           
            UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)

        }
        UISegmentedControl.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor:UIColor.white], for: .selected)
        
        
    }
    
    
    
}
